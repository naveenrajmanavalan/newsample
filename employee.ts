export class Employee
{
    EmployeeID?: number;
    FirstName?: string;
    LastName?: string;
    ContactNo?: number;
    Designation?: string;
}