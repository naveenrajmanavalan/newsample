import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/usermanagement/login/login.component';
import { RegisterComponent } from './components/usermanagement/register/register.component';




const routes: Routes = [ 
    
    {
        path:'login', component:LoginComponent
    },
    {
        path:'register', component:RegisterComponent
    },
]
//export const routing: ModuleWithProviders = RouterModule.forRoot(routes);