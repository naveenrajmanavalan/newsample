import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/usermanagement/login/login.component';
import { RegisterComponent } from './components/usermanagement/register/register.component';
import { ShowemployeeComponent } from './components/service/showemployee/showemployee.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ShowemployeeComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot([
      {path:'' , component:LoginComponent},
      {path:'register' , component:RegisterComponent},
      {path:'show',component: ShowemployeeComponent}
    ]),
 
  ],
  exports: [ RouterModule ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
