import { Component, OnInit } from '@angular/core';
import { Employee } from '../../../../../employee';
import { ApiService } from '../../../api.service';

@Component({
  selector: 'app-showemployee',
  templateUrl: './showemployee.component.html',
  styleUrls: ['./showemployee.component.css']
})
export class ShowemployeeComponent implements OnInit {

  emp: Employee[] | undefined;
  
  constructor(public api: ApiService) { }

  ngOnInit(): void {
  }
  public getEmployee()
  {
  this.api.getEmployee()
  .subscribe(
    (res)=>
    {
      
      console.log('Response success')
      this.emp = res;
    }
  ),
  (err: any)=>
  {
    console.error("Response error")
    alert("Error")
  } 
  }
   

}
