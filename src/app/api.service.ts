import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private nr = "http://api.dotnetawesome.com/api/employee";

  constructor(public http: HttpClient) { }

  getEmployee(): Observable<any>
  {
    return this.http.get(this.nr)
  }
}
